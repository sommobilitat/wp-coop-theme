<?php
class WpCoopTheme {

  function __construct() {
    add_filter('wp_enqueue_scripts', array($this,'add_global_scripts'));
    add_action( 'init', array($this,'register_menus') );
    add_action('after_setup_theme', array($this,'remove_admin_bar'));
    add_action( 'admin_menu', array($this,'remove_admin_menus') );
  }

  function remove_admin_menus(){
    remove_menu_page( 'edit-comments.php' );
  }

  function remove_admin_bar() {
    show_admin_bar(false);
  }

  function add_global_scripts(){
    $temp_uri = get_template_directory_uri();
    wp_enqueue_style('main/css', $temp_uri . '/assets/css/style.css', false, '0.1');
    wp_enqueue_script('main/js', $temp_uri . '/assets/js/main.js',array(), '0.1', true );
    // $glob_js_params = array(
    //  'ajax_url' => admin_url( 'admin-ajax.php' ),
    //  'mapbox_api' => get_field('mapbox_api', 'option'),
    //  'template_dist_images_uri' => get_template_directory_uri()."/src/images/"
    // );
    // wp_localize_script('main/js', 'main_data', $glob_js_params);
  }


  function register_menus() {
    register_nav_menu('primary_menu',__( 'Primary Menu' ));
    register_nav_menu('secondary_menu',__( 'Secondary Menu' ));
  }

}

new WpCoopTheme();