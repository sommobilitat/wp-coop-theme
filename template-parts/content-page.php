<section class="header-image-section"></section>
<div class="page-wrapper" id="web-main-wrapper" style="margin: auto;">
  <main class="main-body" role="main m-auto">
    <?php get_template_part( 'template-parts/header', 'section' ); ?>
    <!-- PAGE CONTENT -->
    <section class="mb-5">
      <?php the_content(); ?>
    </section>
  </main>
</div>