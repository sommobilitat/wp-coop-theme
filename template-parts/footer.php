<footer class="footer">
<div class="page-wrapper">
  <?php
    get_template_part('template-parts/sections/footer','section',$args);
    get_template_part('template-parts/sections/footer_logo','section',$args);
   ?>
</div>
</footer>