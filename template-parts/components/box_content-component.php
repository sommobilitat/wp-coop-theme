<div class="abs-wrapper">
  <?php 
    get_template_part( 'template-parts/components/title','component', $args);
  ?>
  <?php if(isset($args['content'])): ?>
    <p class="big-intro">
      <?php 
        get_template_part( 'template-parts/components/content','component', $args);
      ?>
    </p>
  <br/>
  <?php endif; ?>
  <?php if(isset($args['button_link']) && isset($args['button_title'])): ?>
    <p><a href="<?php echo $args['button_link']; ?>"
        class="button-black-Text scroll-button"><?php echo $args['button_title']; ?><i
        class="fas fa-arrow-right black-button-icon"></i></a></p>
  <?php endif; ?>
</div>