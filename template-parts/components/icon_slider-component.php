<?php 
  $template_directory_uri = get_template_directory_uri();
  $arrow_top_url = $template_directory_uri.'/assets/images/web-arrow-top.png?v=0';
  $arrow_bottom_url = $template_directory_uri.'/assets/images/web-arrow-bottom.png?v=0';
  $arrow_left_url = $template_directory_uri.'/assets/images/web-arrow-left.png?v=0';
  $arrow_right_url = $template_directory_uri.'/assets/images/web-arrow-right.png?v=0';
  if(isset($args['icon_image_1']) && isset($args['icon_image_2']) && isset($args['icon_image_3']) && isset($args['icon_image_4'])):
?>
<div class="circle-text-wrapper">
  <div class="circle-text-wrapper-abs">
    <div class="icon-wrapper icon-top-left icon-wrapper-active" id="top-left">
      <div class="img-wrapper">
        <img src="<?php echo $args['icon_image_1']; ?>"/>
      </div>
    </div>
    <div class="arrow-wrapper arrow-wrapper-left" id="bottom-right">
      <div class="img-wrapper">
        <img src="<?php echo $arrow_left_url; ?>"/>
      </div>
    </div>
    <div class="icon-wrapper icon-top-right" id="top-right">
      <div class="img-wrapper">
        <img src="<?php echo $args['icon_image_2']; ?>"/>
      </div>
    </div>
    <div class="arrow-wrapper arrow-wrapper-top" id="top-right">
      <div class="img-wrapper">
        <img src="<?php echo $arrow_top_url; ?>"/>
      </div>
    </div>
    <div class="icon-wrapper icon-bottom-right" id="bottom-right">
      <div class="img-wrapper">
        <img src="<?php echo $args['icon_image_3']; ?>"/>
      </div>
    </div>
    <div class="arrow-wrapper arrow-wrapper-right" id="bottom-right">
      <div class="img-wrapper">
        <img src="<?php echo $arrow_right_url; ?>"/>
      </div>
    </div>
    <div class="icon-wrapper icon-bottom-left" id="bottom-left">
      <div class="img-wrapper">
        <img src="<?php echo $args['icon_image_4']; ?>"/>
      </div>
    </div>
    <div class="arrow-wrapper arrow-wrapper-bottom" id="bottom-right">
      <div class="img-wrapper">
        <img src="<?php echo $arrow_bottom_url; ?>"/>
      </div>
    </div>
    <?php
      if(isset($args['icon_title_1']) && isset($args['icon_title_2']) && isset($args['icon_title_3']) && isset($args['icon_title_4'])):
    ?>
    <div class="icons-text-wrapper icons-text-wrapper-top-left" id="text-top-left">
      <h4><?php echo $args['icon_title_1']; ?></h4>
    </div>
    <div class="icons-text-wrapper icons-text-wrapper-top-right" style="display: none;" id="text-top-right">
      <h4><?php echo $args['icon_title_2']; ?></h4>
    </div>
    <div class="icons-text-wrapper icons-text-wrapper-bottom-right" style="display: none;" id="text-bottom-right">
      <h4><?php echo $args['icon_title_3']; ?></h4>
    </div>
    <div class="icons-text-wrapper icons-text-wrapper-bottom-left" style="display: none;" id="text-bottom-left">
      <h4><?php echo $args['icon_title_4']; ?></h4>
    </div>
    <?php endif; ?>
  </div>
</div>
<?php endif; ?>