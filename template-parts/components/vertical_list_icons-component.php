<?php if(isset($args['list_contents'])): ?>
    <ul class="no-bullet-list no-padding-list">
        <?php  for ($i = 0; $i < count($args['list_contents']); $i++):?>
        <li class="copy-text mb-3">
            <div class="row">
                <div class="col-2 col-sm-1 col-lg-2">
                    <i class="icon-arrow-list icons-list-contact <?php echo $args['list_icons_class'][$i]; ?>"></i>
                </div>
                <div class="col-10 col-sm-11 col-lg-10">
                    <?php echo $args['list_contents'][$i]; ?>
                </div>
            </div>
        </li>
        <?php endfor; ?>
    </ul>
<?php endif; ?>
