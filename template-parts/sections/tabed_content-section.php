<section class="mb-5 pb-lg-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <?php get_template_part( 'template-parts/components/navigation','component', $args); ?>
      </div>
      <div class="col-lg-9">
        <?php get_template_part( 'template-parts/components/title','component', $args); ?>
        <?php if(isset($args['tabs_title']) && isset($args['tabs_id'])): ?>
        <div class="tab">
          <?php  for ($i = 0; $i < count($args['tabs_title']); $i++):?>
          <button class="anchor tabs-title<?php if($i==0): ?> active-tab<?php endif; ?>" data-attr="<?php echo $args['tabs_id'][$i]; ?>">
            <?php echo $args['tabs_title'][$i]; ?>
          </button>
          <?php endfor; ?>
        </div>
        <?php endif; ?>
        <?php if(isset($args['tabs_content']) && isset($args['tabs_id'])): ?>
        <?php  for ($i = 0; $i < count($args['tabs_title']); $i++):?>
        <div id="<?php echo $args['tabs_id'][$i]; ?>" class="tabcontent">
          <?php echo $args['tabs_content'][$i]; ?>
        </div>
        <?php endfor; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>