<?php $temp_vars = $args; ?>
<section class="mb-5">
  <div class="container">
    <div class="row mb-5">
      <div class="col-lg-3">
        <?php get_template_part( 'template-parts/components/navigation','component', $args); ?>
      </div>
      <div class="col-lg-4 col-md-9">
        <?php
          $args['title'] = $temp_vars['title_col_1'];
          get_template_part( 'template-parts/components/title','component', $args);
          $args['list_icons_class'] = $temp_vars['list_icons_class_col_1'];
          $args['list_contents'] = $temp_vars['list_contents_col_1'];
          get_template_part( 'template-parts/components/vertical_list_icons','component', $args);
        ?>
      </div>
      <div class="mt-3 mt-lg-0 col-lg-4 offset-lg-1 col-md-9">
        <?php
          $args['title'] = $temp_vars['title_col_2'];
          get_template_part( 'template-parts/components/title','component', $args);
          $args['list_icons_class'] = $temp_vars['list_icons_class_col_2'];
          $args['list_contents'] = $temp_vars['list_contents_col_2'];
          get_template_part( 'template-parts/components/vertical_list_icons','component', $args);
        ?>
      </div>
    </div>
  </div>
</section>