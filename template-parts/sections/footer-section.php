<section class="main-footer">
  <div class="container menu-container">
    <div class="row">
      <div class="col-md-3">
        <p class="footer-primary-links"><?php echo get_bloginfo('name'); ?></p>
        <p class="footer-white-text"><?php echo get_bloginfo('description'); ?></p>
      </div>
      <?php 
        $nav_items = wp_get_nav_menu_items('primary-menu');
        if( $nav_items ):
      ?>
      <div class="col-md-3">
      <?php foreach($nav_items as $nav_item):?>
        <div class="footer-primary-links-wrapper">
          <a href="<?php echo $nav_item->url; ?>" class="footer-primary-links scroll-button">
            <?php echo $nav_item->title; ?>
          </a>
        </div>
      <?php endforeach; ?>
      </div>
      <?php endif; ?>
      <?php 
        $nav_items = wp_get_nav_menu_items('secondary-menu');
        if( $nav_items ):
      ?>
      <div class="col-md-3">
        <?php foreach($nav_items as $nav_item):?>
        <div><a href="<?php echo $nav_item->url; ?>" class="footer-secondary-link"><?php echo $nav_item->title; ?></a></div>
        <?php endforeach; ?>
      </div>
      <?php endif; ?>
      <div class="col-md-3">
        <div class="footer-white-text">© 2019 transicioenergetica.cat</div>
      </div>
    </div>
  </div>
</section>
