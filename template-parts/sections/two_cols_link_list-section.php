<section class="mb-5 mtop-avoid-yellowbox">
  <div class="container">
    <div class="row mb-5">
      <div class="col-lg-6">
        <?php get_template_part( 'template-parts/components/navigation','component', $args); ?>
        <?php if(isset($args['links_image'])): ?>
          <div class="row">
            <?php  for ($i = 0; $i < count($args['links_image']); $i++):?>
              <div class="col-4">
                <?php if(isset($args['links_url'][$i])): ?>
                <a href="<?php echo $args['links_url'][$i]; ?>" target="_blank">
                <?php endif; ?>
                <?php if(isset($args['links_image'][$i])): ?>
                <?php $img = get_template_directory_uri()."/assets/images/web-emp-associacioalba.png"; ?>
                  <img class="img-fluid img-link-list" src="<?php echo $args['links_image'][$i]; ?>"/>
                <?php endif; ?>
                <?php if(isset($args['links_url'][$i])): ?>
                </a>
                <?php endif; ?>
              </div>
            <?php endfor; ?>
          </div>
          <?php endif; ?>
      </div>
      <div class="col-lg-6">
        <?php
          get_template_part( 'template-parts/components/title','component', $args);
          get_template_part( 'template-parts/components/content','component', $args);
        ?>
        <?php if(isset($args['links_title'])): ?>
        <ul class="no-bullet-list no-padding-list">
          <?php  for ($i = 0; $i < count($args['links_title']); $i++):?>
          <li class="link-list">
            <i class="fa fa-arrow-right rotate-45 icon-arrow-list"></i>
            <?php if(isset($args['links_url'][$i])): ?>
            <a href="<?php echo $args['links_url'][$i]; ?>" class="img-link-link" target="_blank">
            <?php endif; ?>
            <?php if(isset($args['links_title'][$i])): ?>
              <?php echo $args['links_title'][$i]; ?>
            <?php endif; ?>
            <?php if(isset($args['links_url'][$i])): ?>
            </a>
            <?php endif; ?>
          </li>
          <?php endfor; ?>
        </ul>
        <?php endif; ?>
      </div>
      <?php if(isset($args['image'])): ?>
        <div class="col-sm-12 two-cols-img-mobile">
          <?php get_template_part( 'template-parts/components/image','component', $args); ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>
