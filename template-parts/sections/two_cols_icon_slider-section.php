<?php $temp_vars = $args; ?>
<section class="mb-5">
  <div class="container">
      <div class="row mb-5">
          <div class="col-lg-6">
              <?php get_template_part( 'template-parts/components/navigation','component', $args); ?>
              <div class="row two-cols-icons-circle">
                  <?php get_template_part( 'template-parts/components/icon_slider','component', $args); ?>
              </div>
              
          </div>
          <div class="col-lg-6">
              <?php 
                get_template_part( 'template-parts/components/title','component', $args);
                get_template_part( 'template-parts/components/content','component', $args);
              ?>
          </div>
          <?php if(isset($args['image'])): ?>
          <div class="col-sm-12 two-cols-img-mobile">
            <?php get_template_part( 'template-parts/components/image','component', $args); ?>
          </div>
          <?php endif; ?>
      </div>
  </div>
</section>
