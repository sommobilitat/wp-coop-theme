<!-- GENERAL CONTENT -->
<?php
  $temp_vars = $args;
?>
<section class="mb-5">
  <div class="container">
      <div class="row">
          <div class="col-lg-3">
          <?php
            if(isset($args['menu_title']) && isset($args['id'])):
              get_template_part( 'template-parts/components/navigation','component', $args);
            endif;
          ?>
          </div>
          <div class="col-lg-9">
            <?php
              get_template_part( 'template-parts/components/title', 'component',$args);
              get_template_part( 'template-parts/components/image', 'component',$args);
              get_template_part( 'template-parts/components/content', 'component',$args);
            ?>
            <!--
            {% if button %}
                {% include 'web/components/red-button.html' with button=button %}
            {% endif %}
            -->
          </div>
      </div>
  </div>
</section>