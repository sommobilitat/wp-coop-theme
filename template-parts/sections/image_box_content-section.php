<section class="mb-5 co2-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <?php
          get_template_part( 'template-parts/components/navigation','component', $args);
        ?>
      </div>
    </div>
  </div>
</section>
<section class="mb-5 co2-section">
  <div class="container">
    <?php if(isset($args['image'])): ?>
    <div class="row co2 no-gutters" style='background-image: url("<?php echo $args['image']; ?>"); background-repeat: no-repeat;'>
    </div>
    <?php endif; ?>
    <div class="custom-co2-abs-content">
      <div class="abs-section">
        <div class="row">
          <div class="col-md-6"></div>
          <div class="col-md-6">
            <?php
              get_template_part( 'template-parts/components/box_content','component', $args);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
