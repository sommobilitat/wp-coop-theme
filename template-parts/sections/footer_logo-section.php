<?php if(isset($args['logos_image']) && isset($args['logos_title'])): ?>
<section class="mb-5">
  <div class="container menu-container">
    <div class="row mb-5 mt-5">
      <?php  for ($i = 0; $i < count($args['logos_image']); $i++):?>
      <div class="col-md-4 secondary-footer-col-2">
        <p><strong><?php echo $args['logos_title'][$i]; ?></strong></p>
        <img src="<?php echo $args['logos_image'][$i]; ?>" class="img-fluid" alt="">
      </div>
    <?php endfor; ?>
    </div>
  </div>
</section>
<?php endif; ?>