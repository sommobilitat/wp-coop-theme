<nav class="navbar navbar-expand-lg navbar-web fixed-top">
  <div class="container">
    <a class="navbar-brand" href="/"><img class="img-fluid mainLogo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png?v=2"/></a>
    <?php 
      $nav_items = wp_get_nav_menu_items('primary-menu');
      if( $nav_items ):
    ?>
    <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar" aria-controls="collapsibleNavbar" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
      <ul class="navbar-nav">
        <?php foreach($nav_items as $nav_item):?>
        <li class="nav-item">
          <a class="nav-link menu-link-item" href="<?php echo $nav_item->url; ?>">
            <?php echo $nav_item->title; ?>
          </a>
        </li>
        <?php endforeach;?>
      </ul>
    </div>
    <?php endif;?>
  </div>
</nav>