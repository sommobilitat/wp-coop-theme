<section class="mb-5 pb-lg-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <?php get_template_part( 'template-parts/components/navigation','component', $args); ?>
      </div>
      <div class="col-lg-9">
        <?php get_template_part( 'template-parts/components/title','component', $args); ?>
        <?php if(isset($args['tabs_title']) && isset($args['tabs_id'])): ?>
        <div class="tab">
          <?php  for ($i = 0; $i < count($args['tabs_title']); $i++):?>
          <button class="anchor tabs-title<?php if($i==0): ?> active-tab<?php endif; ?>" data-attr="<?php echo $args['tabs_id'][$i]; ?>">
            <?php echo $args['tabs_title'][$i]; ?>
          </button>
          <?php endfor; ?>
        </div>
        <?php endif; ?>
        <?php if(isset($args['tabs_content']) && isset($args['tabs_id'])): ?>
        <?php  for ($x = 0; $x < count($args['tabs_id']); $x++):?>
            <div class="tabcontent container <?php if($x != 0): echo " d-none"; endif;?>" id="<?php echo $args['tabs_id'][$x]; ?>">
                <div class="row mb-5">
                <div class="col-lg-6">
                    <?php if(isset($args[$args['tabs_id'][$x]]['links_image'])): ?>
                    <div class="row">
                        <?php  for ($i = 0; $i < count($args[$args['tabs_id'][$x]]['links_image']); $i++):?>
                        <div class="col-4">
                            <?php if(isset($args[$args['tabs_id'][$x]]['links_url'][$i])): ?>
                            <a href="<?php echo $args[$args['tabs_id'][$x]]['links_url'][$i]; ?>" target="_blank">
                            <?php endif; ?>
                            <?php if(isset($args[$args['tabs_id'][$x]]['links_image'][$i])): ?>
                            <?php $img = get_template_directory_uri()."/assets/images/web-emp-associacioalba.png"; ?>
                            <img class="img-fluid img-link-list" src="<?php echo $args[$args['tabs_id'][$x]]['links_image'][$i]; ?>"/>
                            <?php endif; ?>
                            <?php if(isset($args[$args['tabs_id'][$x]]['links_url'][$i])): ?>
                            </a>
                            <?php endif; ?>
                        </div>
                        <?php endfor; ?>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="col-lg-6">
                    <?php
                    get_template_part( 'template-parts/components/title','component', $args);
                    get_template_part( 'template-parts/components/content','component', $args);
                    ?>
                    <?php if(isset($args[$args['tabs_id'][$x]]['links_title'])): ?>
                    <ul class="no-bullet-list no-padding-list">
                    <?php  for ($i = 0; $i < count($args[$args['tabs_id'][$x]]['links_title']); $i++):?>
                    <li class="link-list">
                        <i class="fa fa-arrow-right rotate-45 icon-arrow-list"></i>
                        <?php if(isset($args[$args['tabs_id'][$x]]['links_url'][$i])): ?>
                        <a href="<?php echo $args[$args['tabs_id'][$x]]['links_url'][$i]; ?>" class="img-link-link" target="_blank">
                        <?php endif; ?>
                        <?php if(isset($args[$args['tabs_id'][$x]]['links_title'][$i])): ?>
                        <?php echo $args[$args['tabs_id'][$x]]['links_title'][$i]; ?>
                        <?php endif; ?>
                        <?php if(isset($args[$args['tabs_id'][$x]]['links_url'][$i])): ?>
                        </a>
                        <?php endif; ?>
                    </li>
                    <?php endfor; ?>
                    </ul>
                    <?php endif; ?>
                </div>
                </div>
            </div>
        <?php endfor; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>