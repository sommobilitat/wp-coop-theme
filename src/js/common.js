import $ from 'jquery';
const icons_wrapper = $(".icon-wrapper");
const text_icons_wrapper = $(".icons-text-wrapper");
const tabs = $(".tabs-title");
var animating = false;
var animation_interval;

var CommonScript = {

  init: function() {
    if (icons_wrapper.length !== 0 && text_icons_wrapper.length !== 0) {
      CommonScript.circle_text_icon_animation();
      CommonScript.change_to_next_icon();
    }
    tabs.on("click", function (e) {
      let city_name = $(this).attr("data-attr");
      CommonScript.ventajas_tabs(e, city_name);
    });
    /*
    $('.nav-link, .navbar-brand, .scroll-button').click(function () {
      var sectionTo = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(sectionTo).offset().top
      }, 1000);
    });
    $(document).on("click", ".submit-form-participa", function () {
      $('html, body').animate({
        scrollTop: $('#participa').offset().top
      }, 1000);
    });
    */
  },
  ventajas_tabs: function(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].classList.add("d-none");
    }
    tablinks = document.getElementsByClassName("tabs-title");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].classList.remove("active-tab");
    }
    document.getElementById(cityName).classList.remove("d-none");
    evt.currentTarget.classList.add("active-tab");
  },
  change_to_next_icon: function(index = 0) {
    var i = index;
    animation_interval = window.setInterval(function () {
      CommonScript.hover_effect(icons_wrapper, text_icons_wrapper, icons_wrapper[i]);
      i++;
      if (i === 4)
        i = 0;
    }, 2000);
  },
  circle_text_icon_animation: function() {
    icons_wrapper.on("mouseover", function () {
      clearInterval(animation_interval);
      CommonScript.hover_effect(icons_wrapper, text_icons_wrapper, this);
      CommonScript.change_to_next_icon(CommonScript.get_index(icons_wrapper, this));
    });
  },
  get_index: function(icons_obj, cur_element) {
    for (let i = 0; i < icons_obj.length; ++i) {
      if (icons_obj[i].id.includes(cur_element.id))
        return i;
    }
  },
  hover_effect: function(icons_wrapper, text_icons_wrapper, interacted_element) {
    if (!$(interacted_element).hasClass("icon-wrapper-active") && !animating) {
      animating = true;
      // Hover efect
      icons_wrapper.removeClass("icon-wrapper-active");
      $(interacted_element).addClass("icon-wrapper-active");

      let id = $(interacted_element)[0].id;
      let text_ele;
      let current_active = $(".icons-text-wrapper:visible");
      for (let i = 0; i < text_icons_wrapper.length; ++i) {
        if (text_icons_wrapper[i].id.includes(id))
          text_ele = text_icons_wrapper[i];
      }
      if (text_ele !== undefined) {
        $(current_active).fadeOut(300);
        $(text_ele).fadeIn(300, function () {
          animating = false;
        });
      }
    }
  }
};
export default CommonScript;