// import 'bootstrap';
import 'bootstrap/dist/js/bootstrap.min.js';
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'


import arrow_bottom from './images/web-arrow-bottom.png';
import arrow_left from './images/web-arrow-left.png';
import arrow_right from './images/web-arrow-right.png';
import arrow_top from './images/web-arrow-top.png';


// Import the apps styles
import './sass/style.scss';

import './js/main.js'