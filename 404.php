<?php get_header(); ?>
<div class="container-fluid">
  <div class="row">
  <?php
    get_template_part('template-parts/sections/header_page_image', 'section');
    $args = array(
      'title' => '404 - Not Found',
    );
    get_template_part( 'template-parts/sections/header_page', 'section', $args );
    $args = array(
      'content' => '<p>404 - Not Found</p>'
    );
    get_template_part( 'template-parts/sections/general','section', $args);
    $blog_url = get_bloginfo('url');
    $template_directory_uri = get_template_directory_uri();
    $args = array(
      'menu_titles_primary' => array(
        "Qui som?",
        "2a edició",
        "Àmbits de treball",
        "Avantatges",
        "Participa-hi",
        "Contacte"
      ),
      'menu_links_primary' => array(
        $blog_url."/#qui_som",
        $blog_url."/#pilot",
        $blog_url."/#treball",
        $blog_url."/#avantatges",
        $blog_url."/#participa",
        $blog_url."/#contacte"
      ),
      'menu_titles_secondary' => array(
        "Política de privadesa",
        "Cookies",
        "Avís legal"
      ),
      'menu_links_secondary' => array(
        $blog_url."/politica_privacitat",
        $blog_url."/cookies",
        $blog_url."/avis-legal"
      ),
      'logos_image' => array(
        $template_directory_uri."/assets/images/web-economia-social.jpg",
        $template_directory_uri."/assets/images/web-generalitat-departament-treball.jpg",
        $template_directory_uri."/assets/images/web-ministerio-empleo.jpg",
      ),
      'logos_title' => array(
        "Col·labora:",
        "Promou:",
        "Amb el finançament de:"
      )
    );
    get_template_part('template-parts/footer',null,$args);
  ?>
  </div>
</div>
<?php get_footer(); ?>