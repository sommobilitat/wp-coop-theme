<?php get_header(); ?>
<div class="container-fluid">
  <div class="row">
  <?php
    get_template_part('template-parts/sections/header_page_image', 'section');
    while ( have_posts() ) : the_post();
      $page_title = get_the_title();
      $args = array(
        'title' => $page_title,
      );
      get_template_part( 'template-parts/sections/header_page', 'section', $args );
      $args = array(
        'id' => 'page-breadcrumb',
        'menu_title' => $page_title,
        'content' =>  get_the_content()
      );
      get_template_part( 'template-parts/sections/general','section', $args);
    endwhile;
    $blog_url = get_bloginfo('url');
    $template_directory_uri = get_template_directory_uri();
    // $args = array(
    //   'logos_image' => array(
    //     $template_directory_uri."/assets/images/web-economia-social.jpg",
    //     $template_directory_uri."/assets/images/web-generalitat-departament-treball.jpg",
    //     $template_directory_uri."/assets/images/web-ministerio-empleo.jpg",
    //   ),
    //   'logos_title' => array(
    //     "Col·labora:",
    //     "Promou:",
    //     "Amb el finançament de:"
    //   )
    // );
    get_template_part('template-parts/footer');
  ?>
  </div>
</div>
<?php get_footer(); ?>