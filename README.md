# Wp Coop Theme

Wordpress starter theme for cooperative cooperative landing pages.

Use in conjunction with wp-coop-theme-child using the flavour defined on your custom branch.

To develop run:

```
npm install
npm run dev
```

For production run:

```
npm run build
```
on your production server