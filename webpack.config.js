const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
  entry: ['./src/index.js', '../wp-coop-theme-child/src/index.js'],
  output: {
    path: path.resolve(__dirname, 'assets/js'),
    filename: 'main.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [{
          loader: MiniCssExtractPlugin.loader, // inject CSS to page
        }, {
          loader: 'css-loader', // translates CSS into CommonJS modules
        },
        // {
        //   loader: 'postcss-loader', // Run post css actions
        //   options: {
        //     plugins: function () { // post css plugins, can be exported to postcss.config.js
        //       return [
        //         require('autoprefixer')
        //       ];
        //     }
        //   }
        // },
        {
          loader: 'sass-loader' // compiles Sass to CSS
        }]
      },
      { 
        test: /\.(png|jpg|gif)$/,
        use: [
            {
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    context: path.resolve(__dirname, "src/"),
                    outputPath: '../images',
                    // publicPath: './images',
                    useRelativePaths: true
                }
            }
        ]
      },
      {
        test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader"
      },
      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        loader: 'url-loader'
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '../css/style.css',
    }),
    new BrowserSyncPlugin(
      {
        host: 'localhost',
        port: 3000,
        proxy: 'localhost',
        open: 'external',
        files: ['./**/*.php'],
      },
      {
        reload: false,
      }
    ),
  ],
  mode: 'production',
};
